#include "Arduino.h"
#include <Bounce2.h>
#include <Wiegand.h>

#define OFF HIGH
#define ON LOW

#define NONE -1
#define X 1
#define O 0

#define RESET_BUTTON_PIN 23

#define N sizeof valid / sizeof valid[0]

#define PINCHANGE(idx, pin_0, pin_1)                                           \
  void pinStateChanged##idx() {                                                \
    readers[idx].setPin0State(digitalRead(pin_0));                             \
    readers[idx].setPin1State(digitalRead(pin_1));                             \
  }

#define PINSETUP(idx, pin_0, pin_1)                                            \
  pinMode(pin_0, INPUT_PULLDOWN);                                              \
  pinMode(pin_1, INPUT_PULLDOWN);                                              \
  readers[idx].onStateChange(stateChanged, readerIds + idx);                   \
  readers[idx].onReceive(receivedData, readerIds + idx);                       \
  readers[idx].onReceiveError(receivedDataError, readerIds + idx);             \
  readers[idx].begin(Wiegand::LENGTH_ANY, false);                              \
  attachInterrupt(digitalPinToInterrupt(pin_0), pinStateChanged##idx, CHANGE); \
  attachInterrupt(digitalPinToInterrupt(pin_1), pinStateChanged##idx, CHANGE); \
  pinStateChanged##idx();

#define READER_COUNT 9
uint8_t readerIds[READER_COUNT] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
volatile bool readerState[READER_COUNT] = {false, false, false, false, false,
                                           false, false, false, false};
Wiegand readers[READER_COUNT] = {Wiegand(), Wiegand(), Wiegand(),
                                 Wiegand(), Wiegand(), Wiegand(),
                                 Wiegand(), Wiegand(), Wiegand()};
Bounce b = Bounce(); // Instantiate a Bounce object
uint8_t redLedPins[READER_COUNT] = {0, 4, 8, 24, 28, 18, 14, 37, 33};
uint8_t greenLedPins[READER_COUNT] = {1, 5, 9, 25, 29, 19, 15, 38, 34};

long previousMillis = 0;
long interval = 1000;
char squares[READER_COUNT];
uint8_t players[2][Wiegand::MAX_BYTES];
uint8_t turn = O;
uint8_t empty[Wiegand::MAX_BYTES];
uint8_t blink = OFF;

PINCHANGE(0, 2, 3)
PINCHANGE(1, 6, 7)
// 10 and 11 were green then white instead of white then green for that side
PINCHANGE(2, 11, 10)
PINCHANGE(3, 26, 27)
PINCHANGE(4, 30, 31)
PINCHANGE(5, 20, 21)
PINCHANGE(6, 16, 17)
PINCHANGE(7, 39, 12)
PINCHANGE(8, 35, 36)

volatile uint8_t bitLengths[READER_COUNT];
volatile uint8_t credentials[READER_COUNT][Wiegand::MAX_BYTES];

// Notifies when a reader has been connected or disconnected.
// Instead of a message, the seconds parameter can be anything you want --
// Whatever you specify on `wiegand.onStateChange()`
void stateChanged(bool plugged, uint8_t *readerNumber) {
  readerState[*readerNumber] = plugged;
}

// Notifies when a card was read.
// Instead of a message, the third parameter can be anything you want --
// Whatever you specify on `wiegand.onReceive()`
void receivedData(uint8_t *data, uint8_t bits, uint8_t *readerNumber) {
  uint8_t count = (bits + 7) / 8;

  bitLengths[*readerNumber] = bits;
  memcpy((void *)credentials[*readerNumber], data, count);
}

// Notifies when an invalid transmission is detected
void receivedDataError(Wiegand::DataError error, uint8_t *rawData,
                       uint8_t rawBits, uint8_t *readerNumber) {
  uint8_t count = (rawBits + 7) / 8;

  bitLengths[*readerNumber] = rawBits;
  memcpy((void *)credentials[*readerNumber], rawData, count);
}

void error(const __FlashStringHelper *err) {
  // while (1);
}

void setup() {
  PINSETUP(0, 2, 3)
  PINSETUP(1, 6, 7)
  PINSETUP(2, 11, 10)
  PINSETUP(3, 26, 27)
  PINSETUP(4, 30, 31)
  PINSETUP(5, 20, 21)
  PINSETUP(6, 16, 17)
  PINSETUP(7, 39, 12)
  PINSETUP(8, 35, 36)
  for (size_t i = 0; i < READER_COUNT; i++) {
    pinMode(redLedPins[i], OUTPUT);
    digitalWrite(redLedPins[i], OFF);

    pinMode(greenLedPins[i], OUTPUT);
    digitalWrite(greenLedPins[i], OFF);
  }
  b.attach(RESET_BUTTON_PIN, INPUT_PULLUP);
  b.interval(25); // Use a debounce interval of 25 milliseconds

  Serial.begin(115200);

  memset((void *)bitLengths, 0, READER_COUNT);
  memset((void *)credentials, 0, READER_COUNT * Wiegand::MAX_BYTES);
  clearPlayers();
  clearBoard();
}

void clearPlayers() {
  memset((void *)empty, 0, Wiegand::MAX_BYTES);
  memcpy(players[0], (const void *)empty, Wiegand::MAX_BYTES);
  memcpy(players[1], (const void *)empty, Wiegand::MAX_BYTES);
}

void clearBoard() {
  for (size_t i = 0; i < READER_COUNT; i++) {
    squares[i] = (char)NONE;
  }
  turn = O;
  blink = OFF;
}

bool checkWin() {
  bool win = false;

  if (squares[0] == turn && squares[1] == turn && squares[2] == turn) {
    win = true;
  }
  if (squares[3] == turn && squares[4] == turn && squares[5] == turn) {
    win = true;
  }
  if (squares[6] == turn && squares[7] == turn && squares[8] == turn) {
    win = true;
  }
  if (squares[0] == turn && squares[3] == turn && squares[6] == turn) {
    win = true;
  }
  if (squares[1] == turn && squares[4] == turn && squares[7] == turn) {
    win = true;
  }
  if (squares[2] == turn && squares[5] == turn && squares[8] == turn) {
    win = true;
  }
  if (squares[0] == turn && squares[4] == turn && squares[8] == turn) {
    win = true;
  }
  if (squares[2] == turn && squares[4] == turn && squares[6] == turn) {
    win = true;
  }

  return win;
}

void loop() {
  b.update(); // Update the Bounce instance
  for (size_t i = 0; i < READER_COUNT; i++) {
    noInterrupts();
    readers[i].flush();
    if (bitLengths[i] > 0) {
      volatile uint8_t *data = credentials[i];

      // If no player assigned
      if (memcmp(empty, players[turn], Wiegand::MAX_BYTES) == 0) {
        // if this is a different player
        uint8_t other = turn == O ? X : O;
        if (memcmp((const void *)data, players[other], Wiegand::MAX_BYTES) !=
            0) {
          memcpy(players[turn], (const void *)data, Wiegand::MAX_BYTES);
        }
      }

      if (squares[i] == (char)NONE) {

        if (memcmp((const void *)data, players[turn], Wiegand::MAX_BYTES) ==
            0) {
          squares[i] = turn;

          if (checkWin()) {
            for (size_t i = 0; i < READER_COUNT; i++) {
              squares[i] = turn;
            }
          } else {
            if (turn == O) {
              turn = X;
            } else if (turn == X) {
              turn = O;
            }
          }
        }
      }

      // reset
      bitLengths[i] = 0;
    }
    interrupts();
  }

  if (checkWin() == false) {
    for (size_t i = 0; i < READER_COUNT; i++) {
      if (squares[i] == X) {
        digitalWrite(greenLedPins[i], ON);
        digitalWrite(redLedPins[i], OFF);
      } else if (squares[i] == O) {
        digitalWrite(redLedPins[i], ON);
        digitalWrite(greenLedPins[i], OFF);
      } else {
        digitalWrite(redLedPins[i], OFF);
        digitalWrite(greenLedPins[i], OFF);
      }
    }
  }

  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    if (checkWin()) {
      for (size_t i = 0; i < READER_COUNT; i++) {
        if (squares[i] == X) {
          digitalWrite(greenLedPins[i], blink);
          digitalWrite(redLedPins[i], OFF);
        } else if (squares[i] == O) {
          digitalWrite(redLedPins[i], blink);
          digitalWrite(greenLedPins[i], OFF);
        }
      }
      blink = blink == ON ? OFF : ON;
    }
  }

  if (b.fell()) { // Call code if button transitions from HIGH to LOW
    Serial.println("Reset");
    clearBoard();
    clearPlayers();
  }
}
